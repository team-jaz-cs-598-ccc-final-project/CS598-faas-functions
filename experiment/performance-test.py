import boto3
import requests

S3_BUCKET = "cs598-openfaas"
S3_BUCKET_DOWNLOAD_FOLDER = "datasets"
AWS_ACCESS_KEY = "AKIAWCFZCHBVQKJTLY4U"
AWS_SECRET_ACCESS_KEY = "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"


s3 = boto3.client(
    's3',
    aws_access_key_id=AWS_ACCESS_KEY,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY
)

paginator = s3.get_paginator('list_objects')

operation_parameters = {'Bucket': S3_BUCKET,
                        'Prefix': S3_BUCKET_DOWNLOAD_FOLDER}

page_iterator = paginator.paginate(**operation_parameters)

max_count = 500 # Change this to whatever number you need for the experiment (up to 999)
count = 0

for page in page_iterator:
    if count == max_count:
        break

    objects = page['Contents']
    url =  "http://127.0.0.1:8080/async-function/function-1-flask"
    
    for object in page['Contents']:
        if count == max_count:
            break
        count += 1

        file_name = object['Key'].split('/')[1]
        payload = {
            "filename": file_name,
            "callback_url": "http://gateway:8080/async-function/function-2"
        }

        try:
            response = requests.post(url, json=payload)
            if response.status_code != 200 and response.status_code != 202:
                print(f"Failed to notify callback URL. Status Code: {response.status_code}. Response: {response.text}")
            else:
                print(response.content)
        except Exception as e:
            print(f"Error while calling callback URL: {e}")

