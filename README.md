# CS598 CCC Capstone Project - Machine-Learning-Function-Chaining

# Prerequisites - Get Kubernetes Cluster Running 

Please check the README.md in the `/cluster-resources` to follow directions to set up the OpenFaaS system to invoke the functions below.


# Function Definitions for FaaS Function Chaining

Here we define 2 functions that will be chained in a Function as a Service platform.
The functions will be deployed using OpenFaaS.

Each function has its own folder that contains:
- `handler.py`: the function code
- `requirements.txt`: any required dependencies

Each function also has a `yaml` file that describes a Docker container. To build the function, run:

`faas-cli build -f ./<function-name>.yml`

## Function 1
The first function does image processing, which is to resize an input image to a specified size.
The images used here are from [ImageNet-1k](https://huggingface.co/datasets/imagenet-1k/tree/main/data) datasets. Some of the images are stored in an S3 bucket `cs598-openfaas`.
The function will receive an input of filename of an image stored in `s3://cs598-openfaas/datasets/`, which will be used to download the file from S3. Then the function resize the image to max size (100,100). The edited image is then uploaded to the S3 bucket `s3://cs598-openfaas/function-1/`
- Input: file name of the image stored in S3 bucket

How to test the function:
1. Open OpenFaaS dashboard. Go to function-1
2. In the Invoke Function section, type in the filename of the image (`s3://cs598-openfaas/datasets/`) in the Request body as Text. For example: `ILSVRC2012_test_00000002.JPEG`
3. Click on Invoke button.
4. The output image will be stored in `s3://cs598-openfaas/function-1/`


## Function 2
The second function runs an image classification on the input image based on the [ResNet50](https://huggingface.co/microsoft/resnet-50) model.
Similar to function-1, this function receives an input of filename of the edited image from function-1, which is stored in `s3://cs598-openfaas/function-1/`. The function downloads the image from the bucket, then runs the image classification model on it. The result is stored in `s3://cs598-openfaas/function-2/` as a text file.
- Input: updated image filename from S3.

How to test the function:
1. Open OpenFaaS dashboard. Go to function-2
2. In the Invoke Function section, type in the filename of the image (from `s3://cs598-openfaas/function-1/`) in the Request body as Text. For example: `edited_ILSVRC2012_test_00000002.JPEG`
3. Click on Invoke button.
4. The output text file containing the predicted label will be stored in `s3://cs598-openfaas/function-2/`

## S3 bucket Auth
1.User name	Password	Console sign-in URL
cs598-team-jaz-console	cs598-team-jaz-console	https://416999946347.signin.aws.amazon.com/console

## Running Experiment

```
python3 experiment/performance-test.py
```

Note: Change the url and callback url between using `function-async` and `async`

You can also change function-1 to use `function-1-flask` to gather the custom image metrics