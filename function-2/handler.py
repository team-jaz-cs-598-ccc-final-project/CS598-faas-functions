from PIL import Image
from transformers import AutoImageProcessor, ResNetForImageClassification
import torch
import boto3
from botocore.exceptions import NoCredentialsError, ClientError
import io
import json

S3_BUCKET = "cs598-openfaas"
S3_BUCKET_DOWNLOAD_FOLDER = "function-1"
S3_BUCKET_UPLOAD_FOLDER = "function-2"
AWS_ACCESS_KEY = "AKIAWCFZCHBVQKJTLY4U"
AWS_SECRET_ACCESS_KEY = "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"

def upload_file(s3, data, bucket_name, file_path):
    try:
        s3.put_object(
            Body=data,
            Bucket=bucket_name,
            Key=file_path
        )
        print("Upload successful: ", file_path)
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False

def download_file(s3, bucket_name, file_path):
    file_stream = io.BytesIO()
    try:
        s3.download_fileobj(bucket_name, file_path, file_stream)
        print("Download %s successful" % (file_path))
        return Image.open(file_stream)
    except ClientError as err:
        print("Couldn't download file ", file_path)
        print(f"\t{err.response['Error']['Code']}:{err.response['Error']['Message']}")


def handle(req):
    data = json.loads(req)

    # Determine the filename based on the input keys
    if "edited_image_path" in data:
        filename = data["edited_image_path"]
    elif "filename" in data:
        filename = data["filename"]
    else:
        return "Error: Missing expected filename key in input"

    s3 = boto3.client(
        's3',
        aws_access_key_id=AWS_ACCESS_KEY,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

    # Download the edited image using the path
    file_name = filename

    img = download_file(s3, S3_BUCKET, S3_BUCKET_DOWNLOAD_FOLDER + "/" + file_name)

    print("Initializing processor and model...")
    try:
        processor = AutoImageProcessor.from_pretrained("./resnet-50", local_files_only=True)
        model = ResNetForImageClassification.from_pretrained("./resnet-50", local_files_only=True)
    except Exception as e:
         return "Could not process" + str(e)

    print("Passing input image...")
    inputs = processor(img, return_tensors="pt")

    print("Getting prediction...")
    with torch.no_grad():
        logits = model(**inputs).logits

    # Model predicts one of the 1000 ImageNet classes
    predicted_label = logits.argmax(-1).item()
    result = model.config.id2label[predicted_label]
    print("Predicted label: ", result)

    upload_file(s3, result, S3_BUCKET, S3_BUCKET_UPLOAD_FOLDER + "/" + file_name + ".txt")
