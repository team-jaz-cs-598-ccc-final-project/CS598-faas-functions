from PIL import Image
import requests
import json
import boto3
from botocore.exceptions import NoCredentialsError
from botocore.exceptions import ClientError
import io

MAX_SIZE = (100,100)
S3_BUCKET = "cs598-openfaas"
S3_BUCKET_DOWNLOAD_FOLDER = "datasets"
S3_BUCKET_UPLOAD_FOLDER = "function-1"
AWS_ACCESS_KEY = "AKIAWCFZCHBVQKJTLY4U"
AWS_SECRET_ACCESS_KEY = "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"

def upload_file(s3, fileobj, bucket_name, bucket_folder, object_name):
    try:
        file_stream = io.BytesIO()
        fileobj.save(file_stream, format=fileobj.format)
        file_stream.seek(0)

        s3.upload_fileobj(file_stream, bucket_name, '%s/%s' % (bucket_folder,object_name))
        print("Upload successful: ", object_name)
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False
    
def download_file(s3, bucket_name, file_path):
    file_stream = io.BytesIO()
    try: 
        s3.download_fileobj(bucket_name, file_path, file_stream)
        print("Download %s successful" % (file_path))
        return Image.open(file_stream)
    except ClientError as err:
        print("Couldn't download file ", file_path)
        print(f"\t{err.response['Error']['Code']}:{err.response['Error']['Message']}")

def handle(req):
    data = json.loads(req)
    file_name = data["filename"]
    callback_url = data.get("callback_url", None)  # Optional field

    s3 = boto3.client(
        's3',
        aws_access_key_id=AWS_ACCESS_KEY,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

    # Download image from S3 bucket
    img = download_file(s3, S3_BUCKET, S3_BUCKET_DOWNLOAD_FOLDER + "/" + file_name)

    if img is None:  # Add a check to ensure the image was downloaded successfully
        return json.dumps({"status": "error", "message": f"Failed to download {file_name}"})

    width, height = img.size
    print("original_size: ", width, height)

    # If image width or height is larger than MAX_SIZE, resize the image
    if width > MAX_SIZE[0] or height > MAX_SIZE[1]:
        img.thumbnail(MAX_SIZE)
    print("updated size: ", img.size)

    # Upload edited image to S3 bucket
    uploaded = upload_file(s3, img, S3_BUCKET, S3_BUCKET_UPLOAD_FOLDER, "edited_" + file_name)

    if not uploaded:
        return json.dumps({"status": "error", "message": f"Failed to upload edited_{file_name}"})

    # If a callback URL is provided, notify that URL
    if callback_url:
        payload = {
            "filename": "edited_" + file_name,
        }

        try:
            response = requests.post(callback_url, json=payload)
            if response.status_code == 200 or response.status_code == 200:
                content = response.text
                print(content)
            if response.status_code != 200:
                print(f"Failed to notify callback URL. Status Code: {response.status_code}. Response: {response.text}")
        except Exception as e:
            print(f"Error while calling callback URL: {e}")

    return json.dumps({"status": "success",
                       "message": f"Processed and uploaded edited_{file_name}",
                       "edited_image_path": f"{S3_BUCKET_UPLOAD_FOLDER}/edited_{file_name}"
                       })
