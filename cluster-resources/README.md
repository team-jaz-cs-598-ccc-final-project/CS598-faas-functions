# Setting up Minikube and OpenFaaS(Part I)

This guide outlines the steps to set up Minikube and deploy OpenFaaS on Minikube, along with retrieving the admin password for accessing OpenFaaS.

## Prerequisites

Before you begin, ensure you have the following tools installed:
- [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- [Helm](https://helm.sh/docs/intro/install/)

## Step 1: Install Minikube

1. Download and install Minikube from the official website: [Minikube Installation Guide](https://minikube.sigs.k8s.io/docs/start/)

2. Start Minikube:
   ```bash
    minikube start --addons=metrics-server --extra-config=kubelet.housekeeping-interval=10s
## Step 2: Set Up Minikube's Docker Environment
1. Ensure your shell is configured to use Minikube's Docker environment:
    ```bash
    eval $(minikube docker-env)
## Step 3: Install Helm
1. Install Helm, a Kubernetes package manager:

    ```bash
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
    chmod +x get_helm.sh
    ./get_helm.sh

## Step 5: Deploy OpenFaaS
1. Add the OpenFaaS Helm repository:


    ```bash
    helm repo add openfaas https://openfaas.github.io/faas-netes/
2. Create create namespace
    ```bash
    kubectl create namespace openfaas
3. Install OpenFaaS, specifying your release name (replace \<your-release-name\> with your desired name):

    ```bash
    helm upgrade <your-release-name> --install openfaas/openfaas \
    --namespace openfaas \
    --set functionNamespace=openfaas \
    --set basic_auth=true \
    --set image_pull_policy=IfNotPresent \
    -f values.yaml
    --set gateway.directFunctions=false
Replace \<your-release-name\> with your desired release name.

4. Wait for OpenFaaS to deploy:

    ```bash
    kubectl -n openfaas get pods
5. Set up a port-forward to the OpenFaaS gateway if you haven't:

    ```bash
    kubectl port-forward -n openfaas svc/gateway 8080:8080 &
##    Step 5: Retrieve the Admin Password
To access the OpenFaaS admin console, retrieve the admin password:
    ```bash

    echo $(kubectl -n openfaas get secret basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode)

## Step 6: Access OpenFaaS UI
You can now access the OpenFaaS UI using the following URL, and log in with the admin username and the password you retrieved:
    ```bash

    URL: http://127.0.0.1:8080/
    Username: admin
    Password: [The password you retrieved] 
## Step 7: faas-cli credential login(for adding functions later)
    
    faas-cli login --username admin --password <Password> --gateway http://127.0.0.1:8080

# Deploy Autoscaler

## Step 1: Deploy All The Neccessary Resources for Scheduler
```
kubectl apply -f cluster-resources/binpack-scheduler.yaml   
```

## Step 2: Edit Kube-Scheduler Cluster Role
```
kubectl edit clusterrole system:kube-scheduler
```

Add `binpack-scheduler`  to `resourceNames` underneath where you see the default `default-scheduler`

# Setup Functions on OpenFaaS Now (Part II)

## Step 1: Deploy the Function to OpenFaaS:


1. Navigate to our git repo for functions (Amelia) and clone the repo

    ```bash
    git clone https://gitlab.engr.illinois.edu/team-jaz-cs-598-ccc-final-project/CS598-faas-functions.git
2. Go to the dirctory inside
    ```bash
    faas-cli build -f function-1-flask.yml
    faas-cli deploy -f function-1-flask.yml
then we can see the function on openFaaS portal from http://127.0.0.1:8080/ui/
![Alt text](image.png)

## Step 2: Allow Images to Be Pulled In Pods and Add Binpack Scheduler
If you run into the issue ` trying and failing to pull image` when looking at your deploy logs try this out. The reason is the Functions will deploy, but will not start due to `imagePullPolicy` set to `Always`. With this policy, Kubernetes will try to look for a private registry. You can either fix this by setting faas-cli to use a private registry or setting the policy for the deploy to `IfNotPresent`. For the second solution do this:

For example for function-2:
```
kubectl get deployment/function-2 -n openfaas -o yaml > function-2-deployment.yaml 
kubectl apply -f function-2-deployment.yaml
```

Edit the file now on your machine to change the `imagePullPolicy` to `IfNotPresent`. Now the you should see the function status change to ready. 

Also edit `schedulerName` to `binpack-scheduler`

If you need to deploy a newer version of your image after deploying function 2 try,

```
docker tag function-2:latest function-2:{whatever tag you would like}
```

Now change the `image` line in your `function-2-deployment.yaml` to function-2:{whatever tag you would like}. Run these two commands again.

```
kubectl get deployment/function-2 -n openfaas -o yaml > function-2-deployment.yaml 
kubectl apply -f function-2-deployment.yaml
``` 

Now your function status should be Ready. Repeat for any additional functions.

# Deploy async-hpa

## Step 1: Deploy All The Neccessary Resources for Scheduler
```
kubectl apply -f cluster-resources/queue-worker-hpa.yaml  
```

monitor the status of this HPA with:
```
kubectl get hpa queue-worker-hpa -n openfaas
```
```
kubectl describe hpa queue-worker-hpa -n openfaas 
```

# Start Metrics

Go to https://gitlab.engr.illinois.edu/team-jaz-cs-598-ccc-final-project/faas-grafana repo and follow instructions to get grafana metrics running.

## HPA for Function 2
```
kubctl apply -f cluster-resources/function-2-hpa.yaml
```

## Custom Metrics HPA for Function 1

# Update Deployment and ConfigMap

Update the `function-1-flask` deployment, update these values.

```
kubectl get deployment function-1-flask -n openfaas -o yaml >> function-1-flask-deployment.yaml
```

Under `template` `metadata`, add 
```
  template:
    metadata:
      annotations:
        prometheus.io/path: metrics
        prometheus.io/port: "8080"
        prometheus.io/scrape: "true"
```

Then run 
```
kubectl apply -f function-1-flask-deployment.yaml
```

## Additional Config for Function-2


```
kubectl get deployment function-2 -n openfaas -o yaml >> function-2-deployment.yaml
```

Under `template` `metadata`, add 
```
  template:
    metadata:
      annotations:
        prometheus.io/path: metrics
        prometheus.io/port: "8081"
        prometheus.io/scrape: "true"
```

Then run 
```
kubectl apply -f function-2-deployment.yaml
```

# Prometheus UI

To see Prometheus UI, run in a seperate terminal
```
kubectl port-forward -n openfaas svc/prometheus 9090:9090
```

You can see the different metric endpoints at http://localhost:9090/targets?search= and speficially image_size at http://localhost:9090/graph?g0.expr=image_size&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h 

# Prometheus Adapter 

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus-adapter prometheus-community/prometheus-adapter -n openfaas -f values.yaml
```

Try running command below to see what custom metrics are picked up by the Prometheus Adapter 

```
kubectl get --raw /apis/custom.metrics.k8s.io/v1beta1 
kubectl get --raw "/apis/custom.metrics.k8s.io/v1beta1/namespaces/openfaas/pods/*/image_size_avg"
kubectl get --raw "/apis/custom.metrics.k8s.io/v1beta1/namespaces/openfaas/pods/*/http_requests_per_second"
```

Now create hpa for function-1

```
kubctl apply -f cluster-resources/function-1-hpa.yaml
```

# Debugging Issues with Metrics-Server
If you metrics server is having issues picking up metrics, do this"

```
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl get deployment metrics-server -n kube-system  -o yaml > metrics-server.yaml
```

Add this to `args`
```
- --kubelet-insecure-tls
```

kubectl apply -f metrics-server.yaml

# Running Functions 

Synchronously:
```
curl -X POST -H "Content-Type: application/json" -d '{"filename": {filename}, "callback_url": "http://gateway:8080/function/function-2"}' http://localhost:8080/function/function-1  -v
```

Asynchronously:
```
 curl -X POST -H "Content-Type: application/json" -d '{"filename": {filename}, "callback_url": "http://gateway:8080/async-function/function-2"}' http://localhost:8080/async-function/function-1  -v
```

# Additional Debugging Tools

## Inspecting Kubernetes

```
kubectl get pods -n openfaas
kubectl describe pods/{pod name} -n openfaas
```

```
kubectl get deploy -n openfaas
kubectl describe deploy/{deploy name} -n openfaas
```

```
kubectl logs {pod name} -n openfaas
kubectl logs {deploy name} -n openfaas
```

To get the scheduler that assigned the pod to a node

```
 kubectl get pods --all-namespaces -o custom-columns=POD:.metadata.name,NAMESPACE:.metadata.namespace,SCHEDULER:.spec.schedulerName
```
## Inspecting MiniKube Env 

Sometimes images will not be in the minikube env if you have not configured your shell terminal window to use MiniKube's Docker Env. Make sure you do this for each new terminal window. 

```
eval $(minikube docker-env)
```
To make sure your MiniKube Docker Env has the images

```
minikube ssh 
docker images
```

# Setting up Grafana Dashboard

1. Install faas-grafana
2. Go to Dashboard > Import. Then import the new dashboard [openfaas-dash-v1.json](https://gitlab.engr.illinois.edu/team-jaz-cs-598-ccc-final-project/faas-grafana/-/blob/master/grafana/dashboards/openfaas-dash-v1.json?ref_type=heads)
